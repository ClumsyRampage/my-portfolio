import React from 'react';
import { Container, Row, Col, Tab, Nav } from "react-bootstrap";
import { ProjectCard } from "./ProjectCard";
//office
import projImg1 from "../assets/img/project-img1.jpg";
import projImg2 from "../assets/img/project-img2.jpg";
import projImg3 from "../assets/img/project-img3.jpg";
import projImg4 from "../assets/img/project-img4.jpg";
import projImg5 from "../assets/img/project-img5.jpg";
import projImg6 from "../assets/img/project-img6.jpg";
//bike
import bikeImg1 from "../assets/img/bike-img1.jpg";
import bikeImg2 from "../assets/img/bike-img2.jpg";
import bikeImg3 from "../assets/img/bike-img3.jpg";
import bikeImg4 from "../assets/img/bike-img4.jpg";
import bikeImg5 from "../assets/img/bike-img5.jpg";
import bikeImg6 from "../assets/img/bike-img6.jpg";
//gaming
import GamingImg1 from "../assets/img/bike-img1.jpg";
import GamingImg2 from "../assets/img/bike-img2.jpg";
import GamingImg3 from "../assets/img/bike-img3.jpg";
import GamingImg4 from "../assets/img/bike-img4.jpg";
import GamingImg5 from "../assets/img/bike-img5.jpg";
import GamingImg6 from "../assets/img/bike-img6.jpg";

import colorSharp2 from "../assets/img/color-sharp2.png";
import 'animate.css';
import TrackVisibility from 'react-on-screen';

export const Projects = () => {

  const projects1 = [
    {
      title: "Traditional Day",
      description: "Connected to Roots",
      imgUrl: projImg1,
    },
    {
      title: "We Celebrate",
      description: "Not Just Team More Like Family",
      imgUrl: projImg2,
    },
    {
      title: "Meetings",
      description: "Completing Task and Discussions",
      imgUrl: projImg3,
    },
    {
      title: "Awards",
      description: "Work Hard Team Values You",
      imgUrl: projImg4,
    },
    {
      title: "Breast Cancer Awareness",
      description: "Social Awareness Is Our Responsibility",
      imgUrl: projImg5,
    },
    {
      title: "Work is Priority",
      description: "It's Not 9 To 5 It's Upto Work Get's Done",
      imgUrl: projImg6,
    },
  ];

  const projects2 = [
    {
      title: "SHADOW",
      description: "I Love Her She Is Like My Soulmate",
      imgUrl: bikeImg1, // Replace with actual image for biking
    },
    {
      title: "Biker Friends",
      description: "We Explore, We Learn, We Achieve",
      imgUrl: bikeImg2, // Replace with actual image for biking
    },
    {
      title: "Winning",
      description: "Our Heart Don't Beat It Rev's",
      imgUrl: bikeImg3, // Replace with actual image for biking
    },
    {
      title: "We Follow Rules",
      description: "Fun Is Always On But Safety Is Always a Priority",
      imgUrl: bikeImg4, // Replace with actual image for biking
    },
    {
      title: "Bike Enthusiast",
      description: "Passionate About Two-Wheelers",
      imgUrl: bikeImg5, // Replace with actual image for biking
    },
    {
      title: "Biking Events",
      description: "Participating in Cycling Competitions",
      imgUrl: bikeImg6, // Replace with actual image for biking
    },
  ];

  const projects3 = [
    {
      title: "Virtual Realms",
      description: "Conquering Digital Adventures",
      imgUrl: GamingImg1, // Replace with actual image for gaming
    },
    {
      title: "Strategic Play",
      description: "Mastering Tactics and Teamwork",
      imgUrl:  GamingImg2, // Replace with actual image for gaming
    },
    {
      title: "Epic Achievements",
      description: "Celebrating Victories, Building Skills",
      imgUrl:  GamingImg3, // Replace with actual image for gaming
    },
    {
      title: "Gaming Community",
      description: "Connecting With Fellow Gamers",
      imgUrl:  GamingImg4, // Replace with actual image for gaming
    },
    {
      title: "Game Challenges",
      description: "Overcoming In-Game Challenges",
      imgUrl:  GamingImg5, // Replace with actual image for gaming
    },
    {
      title: "Gamer's Delight",
      description: "Enjoying Virtual Worlds",
      imgUrl: projImg6, // Replace with actual image for gaming
    },
  ];

  return (
    <section className="project" id="projects1">
      <Container>
        <Row>
          <Col size={12}>
            <TrackVisibility>
              {({ isVisible }) =>
                <div className={isVisible ? "animate__animated animate__fadeIn" : ""}>
                  <h2>Storyboard</h2>
                  <p>"Explore My Adventures: A Visual Journey"</p>
                  <Tab.Container id="projects-tabs" defaultActiveKey="first">
                    <Nav variant="pills" className="nav-pills mb-5 justify-content-center align-items-center" id="pills-tab">
                      <Nav.Item>
                        <Nav.Link eventKey="first">OFFICE LIFE</Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="second">BIKING</Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="third">GAMING</Nav.Link>
                      </Nav.Item>
                    </Nav>
                    <Tab.Content id="slideInUp" className={isVisible ? "animate__animated animate__slideInUp" : ""}>
                      <Tab.Pane eventKey="first">
                        <Row>
                          {projects1.map((project, index) => (
                            <ProjectCard key={index} {...project} />
                          ))}
                        </Row>
                      </Tab.Pane>
                      <Tab.Pane eventKey="second">
                        <Row>
                          {projects2.map((project, index) => (
                            <ProjectCard key={index} {...project} />
                          ))}
                        </Row>
                      </Tab.Pane>
                      <Tab.Pane eventKey="third">
                        <Row>
                          {projects3.map((project, index) => (
                            <ProjectCard key={index} {...project} />
                          ))}
                        </Row>
                      </Tab.Pane>
                    </Tab.Content>
                  </Tab.Container>
                </div>}
            </TrackVisibility>
          </Col>
        </Row>
      </Container>
      <img className="background-image-right" src={colorSharp2} alt="Background Image"></img>
    </section>
  )
}
